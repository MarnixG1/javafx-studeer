package studeren.view.testscreen;

import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import studeren.view.UISettings;

public class TestScreenView extends BorderPane  {

    private MenuItem exitMI;
    private MenuItem saveMI;
    private MenuItem loadMI;
    private MenuItem settingsMI;
    private MenuItem aboutMI;
    private MenuItem infoMI;
    private UISettings uiSettings;
    private Button vraagB;
    private Button testB;
    private Button afsluitB;
    private VBox centerVB;


    public TestScreenView(UISettings uiSettings) {
        this.uiSettings = uiSettings;
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        this.exitMI = new MenuItem("Exit");
        this.saveMI = new MenuItem("Save");
        this.loadMI = new MenuItem("Load");
        this.settingsMI = new MenuItem("Settings");
        this.aboutMI = new MenuItem("About");
        this.infoMI = new MenuItem("Info");
        this.vraagB = new Button();
        this.testB = new Button();
        this.afsluitB = new Button();
        this.centerVB = new VBox();
    }

    private void layoutNodes() {
        //positionering Menu
        Menu menuFile = new Menu("File",null,loadMI, saveMI, new SeparatorMenuItem(), settingsMI, new SeparatorMenuItem(),exitMI);
        Menu menuHelp = new Menu("Help",null, aboutMI, infoMI);
        MenuBar menuBar = new MenuBar(menuFile,menuHelp);
        setTop(menuBar);
        //positionering center VBox
        this.setCenter(centerVB);
        centerVB.setSpacing(50);
        centerVB.getChildren().setAll(vraagB,testB,afsluitB);
        centerVB.setAlignment(Pos.CENTER);
        centerVB.setVgrow(vraagB, Priority.ALWAYS);
        centerVB.setVgrow(testB, Priority.ALWAYS);
        centerVB.setVgrow(afsluitB, Priority.ALWAYS);
        //layout buttons
        //Vraag Button
        vraagB.setText("Voeg vragen toe");
        vraagB.setMaxWidth(300);
        vraagB.setMaxHeight(50);
        vraagB.setStyle("-fx-font-size:20");
        //test Button
        testB.setText("Test afnemen");
        testB.setMaxWidth(300);
        testB.setMaxHeight(50);
        testB.setStyle("-fx-font-size:20");
        //AfsluitButton
        afsluitB.setText("Afsluiten");
        afsluitB.setMaxWidth(300);
        afsluitB.setMaxHeight(50);
        afsluitB.setStyle("-fx-font-size:20");

    }

    MenuItem getExitItem() {return exitMI;}

    MenuItem getSaveItem() {return saveMI;}

    MenuItem getLoadItem() {return loadMI;}

    MenuItem getSettingsItem() {return settingsMI;}

    MenuItem getAboutItem() {return aboutMI;}

    MenuItem getInfoItem() {return infoMI;}

}
