package studeren.view.addquestionscreen;

import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import studeren.view.UISettings;

public class AddQuestionScreenView extends BorderPane {

    private MenuItem exitMI;
    private MenuItem saveMI;
    private MenuItem loadMI;
    private MenuItem settingsMI;
    private MenuItem aboutMI;
    private MenuItem infoMI;
    private UISettings uiSettings;
    private Button fransB;
    private Button programB;
    private Button hoofdmB;

    public AddQuestionScreenView(UISettings uiSettings) {
        this.uiSettings = uiSettings;
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        this.exitMI = new MenuItem("Exit");
        this.saveMI = new MenuItem("Save");
        this.loadMI = new MenuItem("Load");
        this.settingsMI = new MenuItem("Settings");
        this.aboutMI = new MenuItem("About");
        this.infoMI = new MenuItem("Info");
        this.fransB = new Button();
        this.programB = new Button();
        this.hoofdmB = new Button();
    }

    private void layoutNodes() {
        VBox centerVB = new VBox();
        //positionering Menu
        Menu menuFile = new Menu("File", null, loadMI, saveMI, new SeparatorMenuItem(), settingsMI, new SeparatorMenuItem(), exitMI);
        Menu menuHelp = new Menu("Help", null, aboutMI, infoMI);
        MenuBar menuBar = new MenuBar(menuFile, menuHelp);
        setTop(menuBar);
        //positionering center VBox
        this.setCenter(centerVB);
        centerVB.setSpacing(50);
        centerVB.getChildren().setAll(fransB, programB, hoofdmB);
        centerVB.setAlignment(Pos.CENTER);
        centerVB.setVgrow(fransB, Priority.ALWAYS);
        centerVB.setVgrow(programB, Priority.ALWAYS);
        centerVB.setVgrow(hoofdmB, Priority.ALWAYS);
        //layout buttons
        //Vraag Button
        fransB.setText("Frans");
        fransB.setMaxWidth(300);
        fransB.setMaxHeight(50);
        fransB.setStyle("-fx-font-size:20");
        //test Button
        programB.setText("Programmeren");
        programB.setMaxWidth(300);
        programB.setMaxHeight(50);
        programB.setStyle("-fx-font-size:20");
        //AfsluitButton
        hoofdmB.setText("Terug naar hoofdmenu");
        hoofdmB.setMaxWidth(300);
        hoofdmB.setMaxHeight(50);
        hoofdmB.setStyle("-fx-font-size:20");

    }

    MenuItem getExitItem() {
        return exitMI;
    }

    MenuItem getSaveItem() {
        return saveMI;
    }

    MenuItem getLoadItem() {
        return loadMI;
    }

    MenuItem getSettingsItem() {
        return settingsMI;
    }

    MenuItem getAboutItem() {
        return aboutMI;
    }

    MenuItem getInfoItem() {
        return infoMI;
    }

    public MenuItem getExitMI() {
        return exitMI;
    }

    public Button getFransB() {
        return fransB;
    }

    public Button getProgramB() {
        return programB;
    }

    public Button getHoofdmB() {
        return hoofdmB;
    }
}
