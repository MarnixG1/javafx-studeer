package studeren.view.testselectscreen;

import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import studeren.view.UISettings;

public class TestSelectScreenView extends BorderPane  {

    private MenuItem exitMI;
    private MenuItem saveMI;
    private MenuItem loadMI;
    private MenuItem settingsMI;
    private MenuItem aboutMI;
    private MenuItem infoMI;
    private UISettings uiSettings;
    private CheckBox fransCB;
    private CheckBox programCB;
    private CheckBox statCB;
    private Label fransLabel;
    private Label programLabel;
    private Label statLabel;
    private Label vakLabel;
    private Label aantalLabel;
    private TextField aantalTF;
    private Button startB;
    private Button terugB;



    public TestSelectScreenView(UISettings uiSettings) {
        this.uiSettings = uiSettings;
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        this.terugB = new Button("terug");
        this.exitMI = new MenuItem("Exit");
        this.saveMI = new MenuItem("Save");
        this.loadMI = new MenuItem("Load");
        this.settingsMI = new MenuItem("Settings");
        this.aboutMI = new MenuItem("About");
        this.infoMI = new MenuItem("Info");
        //Vbox met checkboxen
        this.fransCB = new CheckBox();
        this.programCB = new CheckBox();
        this.statCB = new CheckBox();
        //Vbox met Labels
        this.fransLabel = new Label("Frans");
        this.programLabel = new Label("Programmeren");
        this.statLabel = new Label("Statistiek");
        //main Vbox en andere elementen
        this.vakLabel = new Label("Kies uw vak(ken):");
        this.aantalLabel = new Label("Kies uw aantal vragen:");
        this.aantalTF = new TextField();
        this.startB = new Button();

    }

    private void layoutNodes() {
        VBox cbVbox = new VBox();
        VBox lVbox = new VBox();
        HBox hBox =new HBox();
        VBox mainVbox = new VBox();
        //positionering Menu
        Menu menuFile = new Menu("File",null,loadMI, saveMI, new SeparatorMenuItem(), settingsMI, new SeparatorMenuItem(),exitMI);
        Menu menuHelp = new Menu("Help",null, aboutMI, infoMI);
        MenuBar menuBar = new MenuBar(menuFile,menuHelp);
        setTop(menuBar);
        //postitionering&layout main Hbox
        this.setCenter(mainVbox);
        mainVbox.setSpacing(20);
        mainVbox.getChildren().addAll(vakLabel,hBox,aantalLabel,aantalTF,startB);
        mainVbox.setAlignment(Pos.CENTER);
        //layout hbox
        hBox.setSpacing(20);
        hBox.getChildren().addAll(cbVbox,lVbox);
        hBox.setAlignment(Pos.CENTER);
        //layout cbVbox
        cbVbox.setSpacing(20);
        cbVbox.getChildren().addAll(fransCB,programCB,statCB);
        //layout labelVbox
        lVbox.setSpacing(20);
        lVbox.getChildren().addAll(fransLabel,programLabel,statLabel);
        //layout startButton
        startB.setText("Lets fucking Gooo!");
        startB.setMaxWidth(150);
        startB.setMaxHeight(50);
        startB.setStyle("-fx-font-size:14");
        //layout aantal textfield
        aantalTF.setMaxWidth(200);
        //layout terug button
        this.setBottom(terugB);
    }

    MenuItem getExitItem() {return exitMI;}

    MenuItem getSaveItem() {return saveMI;}

    MenuItem getLoadItem() {return loadMI;}

    MenuItem getSettingsItem() {return settingsMI;}

    MenuItem getAboutItem() {return aboutMI;}

    MenuItem getInfoItem() {return infoMI;}

    public MenuItem getExitMI() {
        return exitMI;
    }

    public CheckBox getFransCB() {
        return fransCB;
    }

    public CheckBox getProgramCB() {
        return programCB;
    }

    public CheckBox getStatCB() {
        return statCB;
    }

    public Label getFransLabel() {
        return fransLabel;
    }

    public Label getProgramLabel() {
        return programLabel;
    }

    public Label getStatLabel() {
        return statLabel;
    }

    public Label getVakLabel() {
        return vakLabel;
    }

    public Label getAantalLabel() {
        return aantalLabel;
    }

    public TextField getAantalTF() {
        return aantalTF;
    }

    public Button getStartB() {
        return startB;
    }

    public Button getTerugB() {
        return terugB;
    }
}
