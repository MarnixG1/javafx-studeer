package studeren.view.loginscreen;

import javafx.geometry.Pos;
import javafx.scene.text.Font;
import studeren.view.UISettings;
import javafx.scene.layout.*;
import javafx.scene.control.*;

public class LoginScreenView extends BorderPane  {

    private MenuItem exitMI;
    private MenuItem saveMI;
    private MenuItem loadMI;
    private MenuItem settingsMI;
    private MenuItem aboutMI;
    private MenuItem infoMI;
    private UISettings uiSettings;
    private Label gebruikersnaamL;
    private TextField gebruikersnaamTF;
    private Button gebruikersnaamB;

    public LoginScreenView(UISettings uiSettings) {
        this.uiSettings = uiSettings;
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        this.exitMI = new MenuItem("Exit");
        this.saveMI = new MenuItem("Save");
        this.loadMI = new MenuItem("Load");
        this.settingsMI = new MenuItem("Settings");
        this.aboutMI = new MenuItem("About");
        this.infoMI = new MenuItem("Info");
        this.gebruikersnaamL = new Label("Geef uw gebruikersnaam:");
        this.gebruikersnaamTF = new TextField();
        this.gebruikersnaamB = new Button("Enter");
    }

    private void layoutNodes() {
        VBox centerVB = new VBox();
        //positionering Menu
        Menu menuFile = new Menu("File",null,loadMI, saveMI, new SeparatorMenuItem(), settingsMI, new SeparatorMenuItem(),exitMI);
        Menu menuHelp = new Menu("Help",null, aboutMI, infoMI);
        MenuBar menuBar = new MenuBar(menuFile,menuHelp);
        setTop(menuBar);
        //positionering center Vbox
        this.setCenter(centerVB);
        centerVB.setSpacing(20);
        centerVB.getChildren().addAll(gebruikersnaamL,gebruikersnaamTF,gebruikersnaamB);
        centerVB.setAlignment(Pos.CENTER);
        //layout gebruikersnaam textfield
        gebruikersnaamTF.setMaxWidth(200);
        //layout gebruikersnaam Label
        gebruikersnaamL.setFont(new Font("TimesRoman", 24));
    }

    MenuItem getExitItem() {return exitMI;}

    MenuItem getSaveItem() {return saveMI;}

    MenuItem getLoadItem() {return loadMI;}

    MenuItem getSettingsItem() {return settingsMI;}

    MenuItem getAboutItem() {return aboutMI;}

    MenuItem getInfoItem() {return infoMI;}

    public TextField getGebruikersnaamTF() {
        return gebruikersnaamTF;
    }

    public Button getGebruikersnaamB() {
        return gebruikersnaamB;
    }
}
