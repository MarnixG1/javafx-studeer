package studeren.view.startscreen;

import studeren.model.Model;
import studeren.view.*;
import studeren.view.loginscreen.LoginScreenPresenter;
import studeren.view.loginscreen.LoginScreenView;
import javafx.event.*;
import javafx.scene.control.Alert;
import javafx.stage.WindowEvent;
import java.net.MalformedURLException;

public class StartScreenPresenter {

    private Model model;
    private StartScreenView view;
    private UISettings uiSettings;

    public StartScreenPresenter(Model model, StartScreenView view, UISettings uiSettings) {
        this.model = model;
        this.view = view;
        this.uiSettings = uiSettings;
        updateView();
        EventHandlers();
    }

    private void updateView() {
    }

    private void EventHandlers() {
        view.getTransition().setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                LoginScreenView lsView = new LoginScreenView(uiSettings);
                LoginScreenPresenter lsPresenter = new LoginScreenPresenter(model, lsView, uiSettings);
                view.getScene().setRoot(lsView);
                try {
                    lsView.getScene().getStylesheets().add(uiSettings.getStyleSheetPath().toUri().toURL().toString());
                } catch (MalformedURLException ex) {
                    // // do nothing, if toURL-conversion fails, program can continue
                }
                lsView.getScene().getWindow().sizeToScene();
                lsView.getScene().getWindow().setX(uiSettings.getResX()/20);
                lsView.getScene().getWindow().setY(uiSettings.getResY()/20);
                lsView.getScene().getWindow().setHeight(9 * uiSettings.getResY()/10);
                lsView.getScene().getWindow().setWidth(9 * uiSettings.getResX()/10);
                lsPresenter.windowsHandler();
            }
        });
    }

    public void windowsHandler() {
        view.getScene().getWindow().setOnCloseRequest(new EventHandler<WindowEvent>() {
             @Override
             public void handle(WindowEvent event) {
                 final Alert stopWindow = new Alert(Alert.AlertType.ERROR);
                 stopWindow.setHeaderText("U kan de applicatie nog niet sluiten.");
                 stopWindow.setContentText("Probeer het opnieuw wanneer het programma is opgestart.");
                 stopWindow.showAndWait();
                 event.consume(); } });
    }
}
